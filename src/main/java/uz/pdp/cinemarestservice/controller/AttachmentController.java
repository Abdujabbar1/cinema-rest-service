package uz.pdp.cinemarestservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.cinemarestservice.poyload.ApiResponse;
import uz.pdp.cinemarestservice.service.AttachmentService;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/attachment")
@RequiredArgsConstructor
public class AttachmentController {


    private final AttachmentService attachmentService;

    @GetMapping
    public HttpEntity<?> getAllAttachment() {
        ApiResponse apiResponse = attachmentService.getAllAttachment();
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 204).body(apiResponse);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getAttachment(@PathVariable UUID id) {
        ApiResponse apiResponse = attachmentService.getAttachmentById(id);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/download/{attachmentId}")
    public HttpEntity<?> getAttachmentFile(@PathVariable UUID attachmentId) throws IOException {
        return attachmentService.fileDownload(attachmentId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    public HttpEntity<?> fileUpload(@RequestParam("file") MultipartFile file) {
        ApiResponse apiResponse = attachmentService.fileUpload(file);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/{attachmentId}")
    public HttpEntity<?> editAttachment(@PathVariable UUID attachmentId, @RequestParam("file") MultipartFile file) {
        ApiResponse apiResponse = attachmentService.editAttachment(attachmentId, file);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/{attachmentId}")
    public HttpEntity<?> deleteAttachment(@PathVariable UUID attachmentId) {
        ApiResponse apiResponse = attachmentService.deleteAttachment(attachmentId);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }
}
