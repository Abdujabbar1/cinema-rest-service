package uz.pdp.cinemarestservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinemarestservice.dtos.MovieSessionDto;
import uz.pdp.cinemarestservice.poyload.ApiResponse;
import uz.pdp.cinemarestservice.service.SessionService;
import uz.pdp.cinemarestservice.util.Constants;

@RestController
@RequestMapping("/api/session")
@RequiredArgsConstructor
public class SessionController {

    private final SessionService sessionService;

    @GetMapping
    public HttpEntity<?> getAllSessions(@RequestParam(name = "size", defaultValue = Constants.DEFAULT_PAGE_SIZE) int size,
                                        @RequestParam(name = "page", defaultValue = "1") int page,
                                        @RequestParam(name = "search", defaultValue = "") String search){
        ApiResponse apiResponse = sessionService.getAllMovieSessions(page, size, search);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 204).body(apiResponse);
    }

    @PostMapping
    public HttpEntity<?> addSession(@RequestBody MovieSessionDto movieSessionDto){
        ApiResponse apiResponse = sessionService.addMovieSession(movieSessionDto);
        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 404).body(apiResponse);
    }
}
