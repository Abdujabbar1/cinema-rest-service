package uz.pdp.cinemarestservice.model.enums;

public enum TicketStatus {
    NEW,
    PURCHASE,
    REFUNDED
}
