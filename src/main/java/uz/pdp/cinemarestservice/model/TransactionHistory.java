package uz.pdp.cinemarestservice.model;

import lombok.*;
import uz.pdp.cinemarestservice.model.PayType;
import uz.pdp.cinemarestservice.model.Ticket;
import uz.pdp.cinemarestservice.model.abcClass.AbsEntity;
import uz.pdp.cinemarestservice.model.enums.TicketStatus;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "transaction_histories")
public class TransactionHistory extends AbsEntity {

    @ManyToMany
    @JoinTable(name = "transaction_history_tickets",
            joinColumns = {@JoinColumn(name = "transaction_id")},
            inverseJoinColumns = {@JoinColumn(name = "ticket_id")})
    private List<Ticket> ticket;

    private double amount;

    @OneToOne
    private PayType payType;

    @Enumerated(value = EnumType.STRING)
    private TicketStatus status;

    private String stripePaymentIntent;

    private LocalDate date;


//    @ManyToMany
//    @JoinTable(name = "transaction_history_tickets",
//            joinColumns = @JoinColumn(name = "transaction_id"),
//            inverseJoinColumns = @JoinColumn(name = "tickets"))
//    private List<Ticket> ticket;
//
//    private Double amount;
//    private boolean isRefunded;
//
//    @OneToOne
//    private PayType payType;
//
//
//    private String stripePaymentIntent;
//
//    private LocalDate date;

}
