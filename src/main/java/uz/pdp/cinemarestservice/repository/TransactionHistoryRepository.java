package uz.pdp.cinemarestservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.cinemarestservice.model.TransactionHistory;

import java.util.Optional;
import java.util.UUID;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, UUID> {
    @Query(nativeQuery = true, value = "select ph.*\n" +
            "from transaction_histories ph\n" +
            "         join transaction_history_tickets pht on ph.id = pht.transaction_id\n" +
            "         join tickets t on pht.ticket_id = t.id\n" +
            "where t.id = :ticketId")
    Optional<TransactionHistory> findTransactionHistoryByTicketId(UUID ticketId);

    @Query(nativeQuery = true, value = "select ph.stripe_payment_intent " +
            "from transaction_histories ph " +
            "join transaction_history_tickets phtl on ph.id = phtl.transaction_id" +
            " where phtl.ticket_id = :ticketId")
    String getPaymentIntentByTicketId(UUID ticketId);
}
