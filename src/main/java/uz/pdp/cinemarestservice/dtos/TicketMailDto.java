package uz.pdp.cinemarestservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TicketMailDto {

    private String to;
    private String message;
    private String name;
    private String subject;
}
