package uz.pdp.cinemarestservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReservedHallDto {

    private UUID halId;
    private UUID startDate;
    private UUID endTime;
    private UUID startTime;
}
