package uz.pdp.cinemarestservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.cinemarestservice.dtos.MovieSessionDto;
import uz.pdp.cinemarestservice.dtos.ReservedHallDto;
import uz.pdp.cinemarestservice.model.*;
import uz.pdp.cinemarestservice.poyload.ApiResponse;
import uz.pdp.cinemarestservice.projection.MovieSessionProjection;
import uz.pdp.cinemarestservice.repository.*;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SessionService {

    private final SessionDateRepository dateRepository;
    private final SessionTimeRepository timeRepository;
    private final HallRepository hallRepository;
    private final SessionRepository sessionRepository;
    private final MovieRepository movieRepository;
    private final MovieAnnouncementRepository announcementRepository;


    public ApiResponse getAllMovieSessions(int page, int size, String search) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );
        Page<MovieSessionProjection> all = sessionRepository.findAllSessionsByPage(
                pageable,
                search);
        if (all.isEmpty()) {
            return new ApiResponse("Error!!!", false);
        }
        return new ApiResponse("success", true, all);

    }

    // Add Movie Session
    public ApiResponse addMovieSession(MovieSessionDto sessionDto) {

        try {
            MovieSession movieSession = new MovieSession();

            for (ReservedHallDto reservedHallDto : sessionDto.getReservedHallDtoList()) {

                Optional<MovieAnnouncement> optionalMovieAnnouncement = announcementRepository.findById(sessionDto.getMovieAnnouncementId());
                if (!optionalMovieAnnouncement.isPresent()) {
                    return new ApiResponse("Movie Announcement not found!", false);
                }

                Optional<Hall> optionalHall = hallRepository.findById(reservedHallDto.getHalId());
                if (!optionalHall.isPresent()) {
                    return new ApiResponse("Hall not found!", false);
                }

                Optional<SessionDate> optionalSessionDate = dateRepository.findById(reservedHallDto.getStartDate());
                if (!optionalSessionDate.isPresent()) {
                    return new ApiResponse("Start Date not found!", false);
                }

                Optional<SessionTime> optionalStartSessionTime = timeRepository.findById(reservedHallDto.getStartTime());
                if (!optionalStartSessionTime.isPresent()) {
                    return new ApiResponse("Start Time not found!", false);
                }

                Optional<SessionTime> optionalEndSessionTime = timeRepository.findById(reservedHallDto.getEndTime());
                if (!optionalEndSessionTime.isPresent()) {
                    return new ApiResponse("End Time not found!", false);
                }

                MovieAnnouncement movieAnnouncement = optionalMovieAnnouncement.get();
                Hall hall = optionalHall.get();
                SessionDate startDate = optionalSessionDate.get();
                SessionTime startTime = optionalStartSessionTime.get();
                SessionTime endTime = optionalEndSessionTime.get();

                movieSession.setMovieAnnouncement(movieAnnouncement);
                movieSession.setHall(hall);
                movieSession.setStartDate(startDate);
                movieSession.setStartTime(startTime);
                movieSession.setEndTime(endTime);

            }
            sessionRepository.save(movieSession);
            return new ApiResponse("Success!", true, movieSession);
        } catch (Exception e){
            return new ApiResponse("Error!", false);
        }
    }
}
