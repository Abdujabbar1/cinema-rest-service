package uz.pdp.cinemarestservice.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.cinemarestservice.model.Attachment;
import uz.pdp.cinemarestservice.model.AttachmentContent;
import uz.pdp.cinemarestservice.model.TransactionHistory;
import uz.pdp.cinemarestservice.model.Ticket;
import uz.pdp.cinemarestservice.model.enums.TicketStatus;
import uz.pdp.cinemarestservice.poyload.ApiResponse;
import uz.pdp.cinemarestservice.projection.TicketProjection;
import uz.pdp.cinemarestservice.repository.AttachmentContentRepository;
import uz.pdp.cinemarestservice.repository.AttachmentRepository;
import uz.pdp.cinemarestservice.repository.TransactionHistoryRepository;
import uz.pdp.cinemarestservice.repository.TicketRepository;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class TicketService {

    private final JavaMailSender mailSender;
    private final TicketRepository ticketRepository;
    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository contentRepository;
    private final TransactionHistoryRepository historyRepository;

    public ApiResponse transactionTicket(UUID ticketId) {
        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        if (optionalTicket.isPresent()) {
            Ticket ticket = optionalTicket.get();
            Attachment attachment = getQrCodeAttachment(ticketId);
            ticket.setQrCode(attachment);
            ticket.setStatus(TicketStatus.PURCHASE);
            ticketRepository.save(ticket);
            return new ApiResponse("Success!", true, attachment.getId());
        }
        return new ApiResponse("Not Added!!!", false);
    }

    public Attachment getQrCodeAttachment(UUID ticketId) {
        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        if (optionalTicket.isPresent()) {
            Ticket ticket = optionalTicket.get();
            try {
                byte[] qrCodeImage = new byte[0];
                qrCodeImage = getQrCodeImage(ticket.getId().toString(), 200, 200);
                Attachment attachment = new Attachment();
                attachment.setOriginalName(ticket.getId().toString());
                attachment.setContentType("image/png");
                AttachmentContent attachmentContent = new AttachmentContent(qrCodeImage, attachment);
                attachmentRepository.save(attachment);
                contentRepository.save(attachmentContent);
                return attachment;
            } catch (IOException | WriterException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public byte[] getQrCodeImage(String text, int width, int height) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
        return pngOutputStream.toByteArray();
    }

    public HttpEntity<?> getCurrentUserTickets(UUID userId) {
        List<TicketProjection> allTickets = ticketRepository.getTicketsByUserId(userId);
        return ResponseEntity.ok(allTickets);
    }

    public void changeTicketStatusToTransaction(UUID userId) {
        // TODO: 31.03.2022 user id boicha new ticketlar olinadi va transaction ga ozgartriladi!!!

        List<Ticket> ticketsList = ticketRepository.findByUserIdAndAndStatus(userId, TicketStatus.NEW);

        for (Ticket ticket : ticketsList) {
            ticket.setStatus(TicketStatus.PURCHASE);
        }
        ticketRepository.saveAll(ticketsList);
    }

    public void addTransactionHistory(UUID userId, String paymentIntent) {
        List<Ticket> ticketsList = ticketRepository.findByUserIdAndAndStatus(userId, TicketStatus.NEW);

        Double totalAmount = ticketsList.stream().map(ticket -> ticket.getPrice())
                .collect(Collectors.toList()).stream().mapToDouble(value -> value).sum();
        boolean isRefunded = false;
        TransactionHistory transactionHistory = new TransactionHistory(ticketsList,
                totalAmount,
                null,
                TicketStatus.PURCHASE,
                paymentIntent,
                LocalDate.now());
        historyRepository.save(transactionHistory);
    }

    public HttpEntity<?> sendEmailNotification(String to, UUID ticketId) {

        Ticket ticket = ticketRepository.findById(ticketId).get();

        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom("fayzullakxanovabdujabbar@gmail.com");
        message.setTo(to);
        message.setSubject("Good morning, Abdujabbar!");
//        ticketMailDto.setMessage(String.valueOf(ticket.getMovieSession().getStartDate().getDate()));
        String date = ticket.getMovieSession().getStartDate().getDate().toString();
        String time = ticket.getMovieSession().getStartTime().getTime().toString();
        String name = ticket.getMovieSession().getMovieAnnouncement().getMovie().getTitle();
        String seat = ticket.getSeat().getNumber().toString();
        String hall = ticket.getSeat().getRow().getHall().getName();
        String row = ticket.getSeat().getRow().getNumber().toString();
        String inputMsg = String.format("Your Ticket!\n" +
                        " Date: %s\n" +
                        " Time: %s\n" +
                        " Name: %s\n" +
                        " Seat: %s\n" +
                        " Row: %s\n" +
                        " Hall: %s",
                date, time, name, seat, row, hall);
        message.setText(inputMsg);

        mailSender.send(message);

        return ResponseEntity.ok("Email Successfully!");
    }

}
