package uz.pdp.cinemarestservice.service;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Refund;
import com.stripe.model.checkout.Session;
import com.stripe.param.RefundCreateParams;
import com.stripe.param.checkout.SessionCreateParams;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.cinemarestservice.dtos.TicketDto;
import uz.pdp.cinemarestservice.model.TransactionHistory;
import uz.pdp.cinemarestservice.model.Ticket;
import uz.pdp.cinemarestservice.model.User;
import uz.pdp.cinemarestservice.model.enums.TicketStatus;
import uz.pdp.cinemarestservice.poyload.ApiResponse;
import uz.pdp.cinemarestservice.projection.TicketProjection;
import uz.pdp.cinemarestservice.repository.TransactionHistoryRepository;
import uz.pdp.cinemarestservice.repository.TicketRepository;
import uz.pdp.cinemarestservice.repository.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class TransactionService {

    private final TransactionHistoryRepository historyRepository;
    private final UserRepository userRepository;
    private final TicketRepository ticketRepository;
    @Value("${STRIPE_SK_KEY}")
    String stripeApiKey;

    public ApiResponse createStripeSession() {
        Stripe.apiKey = stripeApiKey;

//=============== KEYINCHALIK @CURRENTUSER GA O'ZGARADI

        User currentUser = userRepository.findByUserName("test");

//===============================
        List<TicketProjection> allTickets = ticketRepository.getTicketsByUserId(currentUser.getId());


        List<SessionCreateParams.LineItem> lineItems = new ArrayList<>();

        for (TicketProjection ticket : allTickets) {
            String movieTitle = ticket.getTitle();
            Double ticketPrice = ticket.getPrice();


            SessionCreateParams.LineItem.PriceData.ProductData productData = SessionCreateParams.LineItem.PriceData.ProductData
                    .builder()
                    .setName(movieTitle)
                    .build();

            SessionCreateParams.LineItem.PriceData priceData = SessionCreateParams.LineItem.PriceData
                    .builder()
                    .setProductData(productData)
                    .setCurrency("usd")
                    .setUnitAmount((long) (ticketPrice * 100))
                    .build();

            SessionCreateParams.LineItem lineItem = SessionCreateParams.LineItem
                    .builder()
                    .setPriceData(priceData)
                    .setQuantity(1L)
                    .build();


            lineItems.add(lineItem);

        }

        SessionCreateParams params = SessionCreateParams
                .builder()
                .setMode(SessionCreateParams.Mode.PAYMENT)
                .setCancelUrl("http://localhost:8080/failed")
                .setSuccessUrl("http://localhost:8080/success?userId=" + currentUser.getId())
                .setClientReferenceId(currentUser.getId().toString())
                .addAllLineItem(lineItems)

                .build();
        try {
            Session session = Session.create(params);
            String checkoutUrl = session.getUrl();


            //returning checkout url of session
            return new ApiResponse("success!", true, checkoutUrl);

        } catch (StripeException e) {
            e.printStackTrace();
        }
        return new ApiResponse("error!", false);

    }

    public HttpEntity<?> refundTicket(List<TicketDto> ticketDtoList) {

        Stripe.apiKey = stripeApiKey;

        String intent = historyRepository.getPaymentIntentByTicketId(ticketDtoList.get(0).getTicketId());

        List<Ticket> ticketList = new ArrayList<>();

        double amount = 0;

        for (TicketDto ticketDto : ticketDtoList) {
            UUID id = ticketDto.getTicketId();
            Ticket ticket = ticketRepository.findById(id).get();
            ticket.setStatus(TicketStatus.REFUNDED);
            amount += ticket.getPrice();
            ticketList.add(ticket);
        }


        RefundCreateParams params = RefundCreateParams
                .builder()
                .setPaymentIntent(intent)
                .setAmount((long) amount)
                .build();

        try {
            Refund refund = Refund.create(params);

            if (refund.getStatus().equals("succeeded")) {
                TransactionHistory transactionHistory = new TransactionHistory(ticketList,
                        amount,
                        null,
                        TicketStatus.REFUNDED,
                        null,
                        LocalDate.now());


                historyRepository.save(transactionHistory);
                return ResponseEntity.ok(refund.getStatus());
            }
        } catch (StripeException e) {
            e.printStackTrace();
        }

        return ResponseEntity.badRequest().build();
    }

//        List<Ticket> refundTickets = null;

//        Refund refund = null;
//        for (TicketDto ticketDto : ticketDtoList) {
//            TransactionHistory transactionHistoryByTicketId = historyRepository.findTransactionHistoryByTicketId(ticketDto.getTicketId()).orElseThrow(() ->
//                    new IllegalStateException("Not found"));
//            String stripePaymentIntent = transactionHistoryByTicketId.getStripePaymentIntent();
//            RefundCreateParams params = RefundCreateParams
//                    .builder()
//                    .setPaymentIntent(stripePaymentIntent)
//                    .build();
//            Ticket ticket = ticketRepository.getById(ticketDto.getTicketId());
//            refundTickets.add(ticket);
//
//            try {
//                refund = Refund.create(params);
//                System.out.println(refund.getStatus());
//                if (refund.getStatus().equals("succeeded")) {
//                    Optional<Ticket> byId = ticketRepository.findById(ticketDto.getTicketId());
//                    Ticket ticket1 = byId.get();
//                    ticket1.setStatus(TicketStatus.REFUNDED);
//                    ticketRepository.save(ticket1);
//                    Optional<TransactionHistory> transactionByTicketId1 = historyRepository.findTransactionHistoryByTicketId(ticketDto.getTicketId());
////                    TransactionHistory transactionHistory = new TransactionHistory(transactionByTicketId1.get().getTicket(), transactionByTicketId1.get().getAmount(),
////                            null,TicketStatus.REFUNDED,stripePaymentIntent, LocalDate.now());
//                    TransactionHistory transactionHistory = transactionByTicketId1.get();
//                    transactionHistory.setStatus(TicketStatus.REFUNDED);
//                    transactionHistory.setId(null);
//                    new TransactionHistory();
//                    historyRepository.save(transactionHistory);
//                    return ResponseEntity.ok(new ApiResponse("successfully refund",true));
//                }
//            } catch (StripeException e) {
//                e.printStackTrace();
//            }
//        }
//        return ResponseEntity.ok(refund.getStatus());


//        Stripe.apiKey = stripeApiKey;
//
//        Refund refund = null;
//        for (TicketDto ticketDto : ticketDtoList) {
//        //    historyRepository.findTransactionHistoryByTicketId()
//            try {
//                TransactionHistory transactionHistoryByTicketId = historyRepository.getPaymentIntentByTicketId(ticketDto.getTicketId()).orElseThrow(() ->
//                        new IllegalStateException("Not found"));
//                String stripePaymentIntent = transactionHistoryByTicketId.getStripePaymentIntent();
//
//                RefundCreateParams params = RefundCreateParams
//                        .builder()
//                        .setPaymentIntent(stripePaymentIntent)
//                        .build();
//
//                refund = Refund.create(params);
//                System.out.println(refund.getStatus());
//
//                if (refund.getStatus().equals("succeeded")) {
//                    Optional<Ticket> byId = ticketRepository.findById(ticketDto.getTicketId());
//                    Ticket ticket = byId.get();
//                    ticket.setStatus(TicketStatus.REFUNDED);
//                    ticketRepository.save(ticket);
//                    Optional<Ticket> ticketsList = ticketRepository.findById(ticket.getId());
//                    List<Ticket> newTicket = (List<Ticket>) ticketsList.get();
//
//                    TransactionHistory transactionHistory = new TransactionHistory(newTicket,100.0,true,null,stripePaymentIntent, LocalDate.now());
//                    historyRepository.save(transactionHistory);
//
//                    // TODO: 31.03.2022 TRANSACTION BOLGAN STATUSNI REFUNDGA OZGARTIRISH!
//                }
//                return ResponseEntity.ok(refund.getStatus());
//            } catch (StripeException e) {
//                e.printStackTrace();
//            }
//        }
//            return ResponseEntity.badRequest().build();
//    }
}
